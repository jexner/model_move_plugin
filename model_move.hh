/*
 * Copyright (C) 2015-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef _GAZEBO_MODEL_MOVE_HH_
#define _GAZEBO_MODEL_MOVE_HH_

#include <vector>

namespace gazebo
{
  /// \brief A plugin to transport a model from point to point using
  /// pose interpolation.
  class ModelMove : public WorldPlugin
  {
    /// \brief Constructor
    public: ModelMove();

    /// \brief Parse goals defined in the SDF
    /// \param[in] _sdf sdf pointer corresponding to goals element
    /// \return True if parsing was succesfull or not
    private: bool LoadGoalsFromSDF(const sdf::ElementPtr _sdf);

    /// \brief Plugin Load function
    /// \param[in] _parent Model pointer to the model defining this plugin
    /// \param[in] _sdf pointer to the SDF of the model
    public: void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf);

    public: void OnUpdate(const common::UpdateInfo &_info);

    /// \brief Pointer to the model that defines this plugin
    private: physics::ModelPtr model;

    /// \brief Starting pose of the path to follow
    private: math::Pose startPose;

    /// \brief Path to follow
    private: std::vector<math::Pose> pathGoals;

    // \biref Pointer to the update event connection
    private: event::ConnectionPtr updateConnection;

    // \brief Point to the world this plugin is attached to.
    private: physics::WorldPtr world;

    // \brief Name of the model that will be moved.
    private: std::string modelName;

    // \brief Velocities.
    private: double transVelocity, rotVelocity;

    // \brief Index of the next goal in pathGoals.
    private: size_t nextGoal;

    // \brief Is the robot already rotated towards the next goal?
    private: bool rotated;

    // \brief The yaw angle that lead to the last goal.
    private: double lastYaw;

    // \brief Sim time when the interpolation started.
    private: gazebo::common::Time interpolationStartTime;

    // \brief Sim time when the model was found.
    private: gazebo::common::Time foundModelTime;

    // \biref Delay before starting interpolation.
    private: double delay;
  };
}
#endif
