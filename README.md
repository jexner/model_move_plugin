# Model Movement Plugin

This is Gazebo 7 plugin is based on the model_move plugin from the [examples](https://bitbucket.org/osrf/gazebo/src/a29d8dabed9a14fb6bafdcd31c84b160c00b127c/examples/plugins/model_move/?at=gazebo7_7.0.0). But instead of using Pose Animation, which is uses splines for interpolation, this plugin makes use of a simple linear interpolation. This plugin is designed for differential kinematic robots:
Interpolation is done in the x-y plane and around the z-axis only!
First, the robot rotates towards the next goal and then "drives" towards it in a straight line.

Another difference is that it is a world plugin because sometimes you don't have direct access to the robot model, e.g. when it is spawned via the `urdf_spawner`. Therefore a model name must be given in the SDF. For usage details see below.

## Build Instructions

From this directory

```
$ mkdir build
$ cd build
$ cmake ../
$ make
```

## Usage

Make sure to have the compiled plugin within the `GAZEBO_PLUGIN_PATH`.

Use the plugin as a child of the world tag:
```
<world>
...
    <plugin name="model_move" filename="libmodel_move.so">
      <model_name>amiro6</model_name> <!-- name of the model -->
      <delay>10</delay> <!-- delay between model discovery and interpolation start -->
      <velocity>
        <translational>0.04</translational> <!-- translation velocity in m/s -->
        <rotational>0.25</rotational> <!-- rotational velocity in rad/s -->
      </velocity>
      <goals>
        <!-- goal poses, only x and y are respected -->
        <pose> 0.702064 -0.351070 0 0 0 0</pose>
        <pose>-0.951424 -0.351070 0 0 0 0</pose>
        <pose>-0.735958 -1.319028 0 0 0 0</pose>
      </goals>
    </plugin>
...
</world>
```
