/*
 * Copyright (C) 2015-2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/transport/transport.hh>
#include <ignition/math/Vector3.hh>
#include <ignition/math/Pose3.hh>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sstream>

#include "model_move.hh"

using namespace gazebo;

// Register this plugin with the simulator
GZ_REGISTER_WORLD_PLUGIN(ModelMove);

/////////////////////////////////////////////////
ModelMove::ModelMove()
{
}

/////////////////////////////////////////////////
bool ModelMove::LoadGoalsFromSDF(const sdf::ElementPtr _sdf)
{
  gzmsg << "[model_move] Processing path goals defined in the SDF file"
        << std::endl;
  GZ_ASSERT(_sdf, "_sdf element is null");

  if (!_sdf->HasElement("goals"))
  {
    gzerr << "[model_move] SDF is missing goals tag" << std::endl;
    return false;
  }

  const sdf::ElementPtr goalSdf = _sdf->GetElement("goals");

  if (!goalSdf->HasElement("pose"))
  {
    gzerr << "[model_move] SDF with goals tag but without pose/s element/s"
          << std::endl;
    return false;
  }

  sdf::ElementPtr poseElem = goalSdf->GetElement("pose");

  while (poseElem)
  {
    this->pathGoals.push_back(poseElem->Get<math::Pose>());
    poseElem = poseElem->GetNextElement("pose");
  }

  GZ_ASSERT(this->pathGoals.size() > 0, "pathGoals should not be zero");

  // read velocities
  if (!_sdf->HasElement("velocity"))
  {
    gzerr << "[model_move] SDF is missing velocities tag" << std::endl;
    return false;
  }
  sdf::ElementPtr velElem = _sdf->GetElement("velocity");

  if (!velElem->HasElement("translational") || !velElem->HasElement("rotational")) {
    gzerr << "[model_move] SDF velocity tag misses translational or rotational" << std::endl;
    return false;
  }

  sdf::ElementPtr transElem = velElem->GetElement("translational");
  this->transVelocity = transElem->Get<double>();
  sdf::ElementPtr rotElem = velElem->GetElement("rotational");
  this->rotVelocity = rotElem->Get<double>();

  if (!_sdf->HasElement("model_name")) {
    gzerr << "[model_move] SDF model name tag missing" << std::endl;
    return false;
  }

  this->modelName = _sdf->GetElement("model_name")->Get<std::string>();

  if (_sdf->HasElement("delay")) {
    this->delay = _sdf->GetElement("delay")->Get<double>();
  } else {
    this->delay = 0;
  }

  gzmsg << "[model_move] loading from SDF sucessfull" << std::endl;
  return true;
}

/////////////////////////////////////////////////
void ModelMove::Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
{
  this->world = _parent;

  // Listen to the update event. This event is broadcast every
  // simulation iteration.
  this->updateConnection = event::Events::ConnectWorldUpdateBegin(
      boost::bind(&ModelMove::OnUpdate, this, _1));

  // Get parameters from sdf
  if (!this->LoadGoalsFromSDF(_sdf))
  {
    gzerr << "[model_move] Problems on loading parameters from sdf "
          << "made the movement impossible" << std::endl;
  }

  // index of next goal
  this->nextGoal = 0;
  // whether the rotation towards the next goal is done
  this->rotated = false;
}

void ModelMove::OnUpdate(const common::UpdateInfo & _info)
{
  // first find the model by name
  if (!this->model) {
    this->model = this->world->GetModel(this->modelName);

    if (!this->model) {
      // still not found
      return;
    } else {
      gzmsg << "[model_move] found model" << std::endl;

      this->startPose = this->model->GetWorldPose();
      this->lastYaw = startPose.rot.GetAsEuler().z;
    }
  }

  // wait before start
  if (_info.simTime < foundModelTime + delay) {
    return;
  }

  // means we're done
  if (this->nextGoal >= this->pathGoals.size())
    return;

  if (this->interpolationStartTime == 0) {
    this->interpolationStartTime = _info.simTime;
  }

  // determine start and end point to interpolate inbetween
  math::Pose start, end;
  if (this->nextGoal == 0)
  {
    start = this->startPose;
    end = this->pathGoals.at(0);
  }
  else
  {
    start = this->pathGoals.at(this->nextGoal - 1);
    end = this->pathGoals.at(this->nextGoal);
  }

  // enforce z and rotation 0, for distance calculation etc.
  start.pos.z = 0;
  start.rot.SetToIdentity();
  end.pos.z = 0;
  end.rot.SetToIdentity();

  // time that has passed since start of this interpolation
  common::Time t = _info.simTime - this->interpolationStartTime;

  // determine angle towards goal
  double goalYaw = atan2(end.pos.y - start.pos.y, end.pos.x - start.pos.x);
  
  if (!this->rotated) {
    math::Angle diffYaw = math::Angle(goalYaw - this->lastYaw);
    diffYaw.Normalize();

    // the expected time needed for this rotation
    common::Time endT = fabs(diffYaw.Radian()) / this->rotVelocity;

    if (t > endT)
    {
      double vel = (diffYaw < 0) ? -this->rotVelocity : this->rotVelocity;
      math::Angle nextYaw = this->lastYaw + vel * t.Double();
      nextYaw.Normalize();

      // we reached the end, continue with translation in this update
      this->rotated = true;
      this->lastYaw = goalYaw;
      // reset time for translation interpolation
      this->interpolationStartTime += endT;
      t = _info.simTime - this->interpolationStartTime;

      math::Vector3 rpy = this->model->GetWorldPose().rot.GetAsEuler();
      math::Pose newPose = math::Pose(start.pos.x, start.pos.y, this->model->GetWorldPose().pos.z,
                                      rpy.x, rpy.y, goalYaw);
      this->model->SetWorldPose(newPose);
      //gzmsg << newPose << std::endl;
    }
    else
    {
      double vel = (diffYaw < 0) ? -this->rotVelocity : this->rotVelocity;
      double nextYaw = this->lastYaw + vel * t.Double();

      math::Vector3 rpy = this->model->GetWorldPose().rot.GetAsEuler();
      math::Pose newPose = math::Pose(start.pos.x, start.pos.y, this->model->GetWorldPose().pos.z,
                                      rpy.x, rpy.y, nextYaw);
      this->model->SetWorldPose(newPose);
      //gzmsg << newPose << std::endl;
      return; // done for this update
    }
  }

  // translation
  // the expected time needed for this translation
  common::Time endT = start.pos.Distance(end.pos) / this->transVelocity;

  if (t > endT)
  {
    math::Vector3 rpy = this->model->GetWorldPose().rot.GetAsEuler();
    math::Pose newPose(end.pos.x, end.pos.y, this->model->GetWorldPose().pos.z,
                       rpy.x, rpy.y, goalYaw);
    this->model->SetWorldPose(newPose);
    //gzmsg << newPose << std::endl;

    this->nextGoal++;
    this->rotated = false;
    this->interpolationStartTime += endT;
  }
  else
  {
    double nextX = start.pos.x + cos(goalYaw) * this->transVelocity * t.Double();
    double nextY = start.pos.y + sin(goalYaw) * this->transVelocity * t.Double();

    math::Vector3 rpy = this->model->GetWorldPose().rot.GetAsEuler();
    math::Pose newPose(nextX, nextY, this->model->GetWorldPose().pos.z,
                       rpy.x, rpy.y, goalYaw);
    this->model->SetWorldPose(newPose);
    //gzmsg << newPose << std::endl;
  }

}


